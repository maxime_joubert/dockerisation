<?php

namespace App\Entity;

use App\Repository\IdeaRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=IdeaRepository::class)
 */
class Idea
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=250, nullable=false)
     * @Assert\NotBlank(message="Veuillez entrer un titre.")
     * @Assert\Length(
     *      min = 1,
     *      max = 250,
     *      minMessage = "Vous devez rentrer au moins {{ limit }} caractères.",
     *      maxMessage = "Vous ne pouvez pas rentrer plus de {{ limit }} caractères."
     * )
     */
    private $title;

    /**
     * @ORM\Column(type="string", length=255, nullable=false)
     * @Assert\NotBlank(message="Veuillez entrer une description.")
     * @Assert\Length(
     *      min = 1,
     *      max = 255,
     *      minMessage = "Vous devez rentrer au moins {{ limit }} caractères.",
     *      maxMessage = "Vous ne pouvez pas rentrer plus de {{ limit }} caractères."
     * )
     */
    private $description;

    /**
     * @ORM\Column(type="string", length=255, nullable=false)
     * @Assert\NotBlank(message="Veuillez entrer un nom d'auteur.")
     * @Assert\Length(
     *      min = 1,
     *      max = 255,
     *      minMessage = "Vous devez rentrer au moins {{ limit }} caractères.",
     *      maxMessage = "Vous ne pouvez pas rentrer plus de {{ limit }} caractères."
     * )
     */
    private $author;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isPublished;

    /**
     * @ORM\Column(type="date", nullable=false)
     */
    private $dateCreated;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(?string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getAuthor(): ?string
    {
        return $this->author;
    }

    public function setAuthor(?string $author): self
    {
        $this->author = $author;

        return $this;
    }

    public function getIsPublished(): ?bool
    {
        return $this->isPublished;
    }

    public function setIsPublished(bool $isPublished): self
    {
        $this->isPublished = $isPublished;

        return $this;
    }

    public function getDateCreated(): ?\DateTimeInterface
    {
        return $this->dateCreated;
    }

    public function setDateCreated(?\DateTimeInterface $dateCreated): self
    {
        $this->dateCreated = $dateCreated;

        return $this;
    }

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Category", inversedBy="ideas")
     */

    private $Category;

    /**
     * @return mixed
     */
    public function getCategory()
    {
        return $this->Category;
    }

    /**
     * @param mixed $Category
     */
    public function setCategory($Category): void
    {
        $this->Category = $Category;
    }

}
