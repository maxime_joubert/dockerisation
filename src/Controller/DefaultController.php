<?php

namespace App\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class DefaultController extends AbstractController {

    /**
     * @Route("/", name="index")
     **/

    public function index() {
        return $this->render("default/home.html.twig");
    }

    /**
     * @Route("/about-us", name="about-us")
     **/

    public function about() {
        return $this->render("default/about.html.twig");
    }

}