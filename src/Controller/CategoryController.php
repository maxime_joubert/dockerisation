<?php

namespace App\Controller;

use App\Entity\Category;
use App\Entity\Idea;
use App\Form\CategoryType;
use App\Form\IdeaType;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * @Route ("/category")
 */

class CategoryController extends AbstractController {

    /**
     * @Route("/add", name="add_category")
     **/

    public function add(EntityManagerInterface $em, Request $request) {
        $this->denyAccessUnlessGranted("ROLE_ADMIN");

        $category = new Category();
        $categoryForm =$this->createForm(CategoryType::class, $category);
        $categoryForm->handleRequest($request);

        if ($categoryForm->isSubmitted() && $categoryForm->isValid()) {
            $em->persist($category);
            $em->flush();
            $name = $category->getName();
            $this->addFlash("success", "La catégorie $name vient d'être ajoutée.");

            return $this->redirectToRoute("modify_category", [
                "id" => $category->getId()
            ]);
        }

        return $this->render("category/add.html.twig", [
            "categoryForm" => $categoryForm->createView()
        ]);
    }

    /**
     * @Route("/list", name="list_category")
     **/

    public function list() {
        $this->denyAccessUnlessGranted("ROLE_ADMIN");

        $categoriesRepo = $this->getDoctrine()->getRepository(Category::class);
        $categories = $categoriesRepo->findAll();

        return $this->render("category/list.html.twig", [
            "categories" => $categories
        ]);
    }

    /**
     * @Route("/modify/{id}", name="modify_category")
     **/

    public function modify(Request $request, Category $category): Response {
        $this->denyAccessUnlessGranted("ROLE_ADMIN");

        $form = $this->createForm(CategoryType::class, $category);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('list_category', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('category/modify.html.twig', [
            'category' => $category,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="category_delete", methods={"POST"})
     */
    public function delete(Request $request, Category $category): Response
    {
        $this->denyAccessUnlessGranted("ROLE_ADMIN");

        if ($this->isCsrfTokenValid('delete'.$category->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($category);
            $entityManager->flush();
        }

        return $this->redirectToRoute('list_category', [], Response::HTTP_SEE_OTHER);
    }

}