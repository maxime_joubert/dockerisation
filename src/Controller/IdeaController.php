<?php

namespace App\Controller;

use App\Entity\Idea;
use App\Form\Idea1Type;
use App\Form\IdeaType;
use App\Repository\IdeaRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route ("/idea")
 */

class IdeaController extends AbstractController
{
    /**
     * @Route("/list", name="list")
     **/

    public function list() {
        $ideasRepo = $this->getDoctrine()->getRepository(Idea::class);
        $ideas = $ideasRepo->findAll();

        return $this->render("default/list.html.twig", [
            "ideas" => $ideas
        ]);
    }

    /**
     * @Route("/add", name="add")
     **/

    public function add(EntityManagerInterface $em, Request $request) {
        $this->denyAccessUnlessGranted("ROLE_USER");

        $idea = new Idea();
        $ideaForm =$this->createForm(IdeaType::class, $idea);
        $ideaForm->handleRequest($request);

        if ($ideaForm->isSubmitted() && $ideaForm->isValid()) {
            $idea->setIsPublished(true);
            $idea->setDateCreated(new \DateTime());
            $em->persist($idea);
            $em->flush();
            $title = $idea->getTitle();
            $this->addFlash("success", "L'idée $title vient d'être ajoutée.");

            return $this->redirectToRoute("detail", [
                "id" => $idea->getId()
            ]);
        }

        return $this->render("default/add.html.twig", [
            "ideaForm" => $ideaForm->createView()
        ]);
    }

    /**
     * @Route("/detail/{id}", name="detail", requirements={"id": "\d+"}, methods={"GET"})
     **/

    public function detail($id, Request $request) {
        $ideaRepo = $this->getDoctrine()->getRepository(Idea::class);
        $idea = $ideaRepo->find($id);

        return $this->render("default/detail.html.twig", [
            "idea" => $idea
        ]);
    }

    /**
     * @Route("/{id}/edit", name="idea_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Idea $idea): Response
    {
        $this->denyAccessUnlessGranted("ROLE_USER");

        $user = $this->get('security.token_storage')->getToken()->getUser();

        if ($idea->getAuthor() !== $user->getUsername()) {
            return $this->redirectToRoute("list");
        }

        $form = $this->createForm(IdeaType::class, $idea);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('list', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('idea/edit.html.twig', [
            'idea' => $idea,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="idea_delete", methods={"POST"})
     */
    public function delete(Request $request, Idea $idea): Response
    {
        if ($this->isCsrfTokenValid('delete'.$idea->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($idea);
            $entityManager->flush();
        }

        return $this->redirectToRoute('list', [], Response::HTTP_SEE_OTHER);
    }

}